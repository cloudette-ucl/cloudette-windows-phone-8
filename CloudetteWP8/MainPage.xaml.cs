﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Sockets;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Navigation;
using Microsoft.Phone.Controls;
using Microsoft.Phone.Shell;
using CloudetteWP8.Resources;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Threading;
using System.Windows.Shapes;
using System.Data;
using System.IO;

namespace CloudetteWP8
{
    public partial class MainPage : PhoneApplicationPage
    {
        // Constructor
        public MainPage()
        {
            InitializeComponent();
            //ping Cloudette
            //ping Google
            //search for access points
     
            //choose connection utility or file browser
            title.Text = "Hello!";
            connectPrompt.Text = "You can either view the files on your Cloudette, or set up an internet connection";
            Button1.Opacity = 1;
            Button1.Content = "View Files";
            Button1.IsEnabled = true;
            Button2.Opacity = 1;
            Button2.IsEnabled = true;
            Button2.Content = "Connect";
            loadingBar.Opacity = 0;
            currentState = "start";
            //obtainSSIDs();
            //bring up list view

            // Sample code to localize the ApplicationBar
            //BuildLocalizedApplicationBar();
        }
        

        private async void obtainSSIDs()
        {
            TCPClient tcpc = new TCPClient();
            tcpc.start("192.168.42.1", 6789, "SCAN\n");
            await Task.Delay(8000);
            String response = tcpc.response;
            response = response.Replace("NEWLINE", "\n");
            ParseScanInformation psi = new ParseScanInformation();
            psi.parseInformation(response);
            populateListView(psi);
            loadingBar.Opacity = 0;
            loadingBar.IsEnabled = false;
            //NavigationService.Navigate(new Uri("/FileList.xaml", UriKind.RelativeOrAbsolute));
            setUpUIChooseAP();
        }

        private void setUpUIChooseAP()
        {
            title.Text = "Alright, let's go!";
            connectPrompt.Text = "Where would you like to connect to?";
            Button1.Opacity = 1;
            Button1.IsEnabled = true;
            Button1.Content = "Search Again";
            Button2.Content = "Confirm";
            Button2.Opacity = 1;
            Button2.IsEnabled = true;
            currentState = "choose access point";
        }

        private void populateListView(ParseScanInformation psi)
        {
            for (int i = 0; i < psi.SSID.Count; i ++)
            {
                CustomListItem cli = new CustomListItem();
                cli.ssidName.Text = (psi.SSID.ElementAt(i));
                cli.ssidStrength.Value = Int32.Parse((psi.Quality.ElementAt(i)));
                cli.Width = this.ActualWidth;
                SSIDs.Items.Add(cli);
            }
        }

        private String currentState = String.Empty;

        private String currentChoice = String.Empty;

        private void SSIDs_SelectionChanged(object sender, SelectionChangedEventArgs e)
        {
            
            for (int i = 0; i < SSIDs.Items.Count; i++)
            {
                CustomListItem cli = (CustomListItem)SSIDs.Items.ElementAt(i);
                
                cli.ssidName.Foreground = new SolidColorBrush(Colors.White);
            }
            if (SSIDs.SelectedIndex >= 0 && SSIDs.SelectedIndex <= SSIDs.Items.Count - 1)
            {
                CustomListItem cli2 = (CustomListItem)SSIDs.Items.ElementAt(SSIDs.SelectedIndex);
                currentChoice = cli2.ssidName.Text;
                cli2.ssidName.Foreground = new SolidColorBrush(Colors.Yellow);
            }
          
        }

        private void Button1_Click(object sender, RoutedEventArgs e)
        {
            switch(currentState)
            {
                case ("start"):
                    NavigationService.Navigate(new Uri("/FileList.xaml", UriKind.RelativeOrAbsolute));
                    break;
                case ("choose access point"):
                    currentChoice = string.Empty;
                    title.Text = "Let's get started!";
                    connectPrompt.Text = "We are connecting your phone to your Cloudette. Give us a sec!";
                    Button1.Opacity = 0;
                    SSIDs.Opacity = 1;
                    SSIDs.IsEnabled = true;
                    loadingBar.Opacity = 1;
                    loadingBar.IsEnabled = true;
                    Button1.IsEnabled = false;
                    Button2.IsEnabled = false;
                    Button2.Opacity = 0;
                    SSIDs.Items.Clear();
                    obtainSSIDs();
                    System.GC.Collect();
                    break;
                case ("enter passkey"):
                    passkeyEntry.IsEnabled = false;
                        passkeyEntry.Opacity = 0;
                    currentChoice = string.Empty;
                    SSIDs.Opacity = 1;
                    SSIDs.IsEnabled = true;
                    currentState = "choose access point";
                    Button1.Opacity = 0;
                    loadingBar.Opacity = 1;
                    title.Text = "Let's get started!";
                    connectPrompt.Text = "We are connecting your phone to your Cloudette. Give us a sec!";
                    loadingBar.IsEnabled = true;
                    Button1.IsEnabled = false;
                    Button2.IsEnabled = false;
                    Button2.Opacity = 0;
                    SSIDs.Items.Clear();
                    obtainSSIDs();
                    System.GC.Collect();
                    break;
            }
        }

        private void Button2_Click(object sender, RoutedEventArgs e)
        {
            switch (currentState)
            {
                case ("start"):
                    obtainSSIDs();
                    Button1.IsEnabled = false;
                    Button1.Opacity = 0;
                    Button2.IsEnabled = false;
                    Button2.Opacity = 0;
                    loadingBar.Opacity = 1;
                    title.Text = "Let's get started!";
                    connectPrompt.Text = "We are connecting your phone to Cloudette. Give us a sec!";
                    break;
                case ("choose access point"):
                    if (currentChoice != string.Empty)
                    {
                        currentState = "enter passkey";
                        SSIDs.IsEnabled = false;
                        SSIDs.Opacity = 0;
                        title.Text = "We're getting there!";
                        connectPrompt.Text = "Please enter your passkey below";
                        passkeyEntry.IsEnabled = true;
                        passkeyEntry.Opacity = 1;
                       
                    }
                    break;
                case("enter passkey"):
                    String passkey = passkeyEntry.Password;
                    GenerateInterfacesFile gif = new GenerateInterfacesFile();
                    gif.generateInterfacesFiles(currentChoice, passkey);
                    loadingBar.IsEnabled = true;
                    loadingBar.Opacity = 1;
                    title.Text = "Almost there..";
                    connectPrompt.Text = "We're just connecting you to your chosen access point right now";
                    Button1.IsEnabled = false;
                    Button2.IsEnabled = false;
                    Button1.Opacity = 0;
                    passkeyEntry.Opacity = 0;
                    passkeyEntry.IsEnabled = false;
                    Button2.Opacity = 0;
                    sendInterfacesFile();
                    SSIDs.Items.Clear();
                    System.GC.Collect();
                    launchOneDriveAuth();
                    break;
            }
        }

        private async void launchOneDriveAuth()
        {
            await Task.Delay(30000);
            NavigationService.Navigate(new Uri("/OneDriveAuth.xaml", UriKind.RelativeOrAbsolute));
        }

        private void sendInterfacesFile()
        {
            TCPClient tcp = new TCPClient();
            tcp.start("192.168.42.1", 6789, "CHOSENSSID\n");
            String interfacesContent = File.OpenText("interfaces").ReadToEnd();
            tcp.start("192.168.42.1", 9876, interfacesContent );
            interfacesContent = string.Empty;
        }

        private void passkeyEntry_KeyDown(object sender, KeyEventArgs e)
        {
            if (e.Key == Key.Enter)
            {
                Button2_Click(sender, e);
            }
        }

        /// <summary>
        /// Closes the Socket connection and releases all associated resources
        /// </summary>
        

    

         

        // Sample code for building a localized ApplicationBar
        //private void BuildLocalizedApplicationBar()
        //{
        //    // Set the page's ApplicationBar to a new instance of ApplicationBar.
        //    ApplicationBar = new ApplicationBar();

        //    // Create a new button and set the text value to the localized string from AppResources.
        //    ApplicationBarIconButton appBarButton = new ApplicationBarIconButton(new Uri("/Assets/AppBar/appbar.add.rest.png", UriKind.Relative));
        //    appBarButton.Text = AppResources.AppBarButtonText;
        //    ApplicationBar.Buttons.Add(appBarButton);

        //    // Create a new menu item with the localized string from AppResources.
        //    ApplicationBarMenuItem appBarMenuItem = new ApplicationBarMenuItem(AppResources.AppBarMenuItemText);
        //    ApplicationBar.MenuItems.Add(appBarMenuItem);
        //}
    }
}