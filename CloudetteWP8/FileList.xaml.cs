﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Windows;
using Windows.Phone.Storage.SharedAccess;
using Windows.Phone.Storage;
using Windows.Storage;
using System.Windows.Controls;
using System.Windows.Media.Imaging;
using System.Net.Sockets;
using System.Windows.Navigation;
using Microsoft.Phone.Controls;
using System.IO.IsolatedStorage;
using Microsoft.Phone.Shell;
using System.Threading.Tasks;
using System.Windows.Media;
using System.Diagnostics;
using System.Text;
using System.IO;
using Newtonsoft.Json.Linq;
using System;
using System.Collections.Generic;
using Windows.ApplicationModel.Core;
using Windows.Phone.Storage.SharedAccess;
using Windows.Networking;
using Windows.Networking.Connectivity;
using Windows.Networking.Sockets;
using Windows.Storage.Streams;
using Windows.UI.Core;



namespace CloudetteWP8
{
    public partial class FileList : PhoneApplicationPage
    {
        private String currentDirectory = "/media/CLOUDETTE/";
        public FileList()
        {
            InitializeComponent();
            title.Text = "CLOUDETTE".ToUpper();
     
            getFolder(currentDirectory);
        }

         BitmapImage tn1 = new BitmapImage();
         BitmapImage tn2 = new BitmapImage();

         List<int> fileSizes = new List<int>();
         
        private void buttonDisable()
        {
             selectButton.IsEnabled = false;
             backButton.IsEnabled = false;
        }

        private void buttonEnable()
        {
            selectButton.IsEnabled = true;
            backButton.IsEnabled = true;
        }

        private async void getFolder(String directory)
        {
            buttonDisable();
            tn1.SetSource(Application.GetResourceStream(new Uri(@"Assets/fileicon.png", UriKind.Relative)).Stream);
            tn2.SetSource(Application.GetResourceStream(new Uri(@"Assets/foldericon.png", UriKind.Relative)).Stream);
            String query = "LIST " + directory + "\n";
            TCPClient tcp = new TCPClient();
            tcp.start("192.168.42.1", 6789, query);
            await Task.Delay(8000);
            String response = tcp.response;
            JArray json = JArray.Parse(response);
            for (int i = 0; i < json.Count; i++)
            {
                FileListControl flc = new FileListControl();
                if (json.ElementAt(i).ToString().Contains("File"))
                {
                    int index = json.ElementAt(i).ToString().IndexOf("Size");
                    String fileName = json.ElementAt(i).ToString().Substring(6,index-7);
                    //fileName = json.ElementAt(i).ToString().Substring(6, fileName.Length-index);
                    flc.icon.Source = tn1;
                    flc.fileName.Text = fileName;
                   
                    String fileSize = json.ElementAt(i).ToString().Substring(index, json.ElementAt(i).ToString().Length - index);
                    fileSize = fileSize.Substring(6, fileSize.Length - 6);
                    fileSizes.Add(int.Parse(fileSize));
                }
                else
                {
                    String folderName = json.ElementAt(i).ToString().Substring(8);
                    flc.icon.Source = tn2;
                    flc.fileName.Text = folderName;
                    fileSizes.Add(0);
                }
               
               
                fileList.Items.Add(flc);
            }
            buttonEnable();

        }

        String currentChoice = String.Empty;
        String currentType = String.Empty;
        int currentIndex = 0;
        private void fileList_SelectionChanged(object sender, SelectionChangedEventArgs e)
        {
            for (int i = 0; i < fileList.Items.Count; i++)
            {
                FileListControl cli = (FileListControl)fileList.Items.ElementAt(i);
                cli.fileName.Foreground = new SolidColorBrush(Colors.White);
            }
            if (fileList.SelectedIndex >= 0 && fileList.SelectedIndex <= fileList.Items.Count - 1)
            {
                FileListControl cli2 = (FileListControl)fileList.Items.ElementAt(fileList.SelectedIndex);
                currentChoice = cli2.fileName.Text;
                if (cli2.icon.Source.Equals(tn1))
                {
                    currentType = "file";
                    //file operation
                }
                else
                {
                    currentType = "folder";
                }
                currentIndex = fileList.SelectedIndex;
                cli2.fileName.Foreground = new SolidColorBrush(Colors.Yellow);
            }
        }

        private async void Select_Click(object sender, RoutedEventArgs e)
        {
            if (currentType.Equals("folder"))
            {
                fileList.Items.Clear();
                currentDirectory = currentDirectory + currentChoice + "/";
                title.Text = currentChoice.ToUpper();
                getFolder(currentDirectory);
            }
            else
            {
                List<byte> received = new List<byte>();
                String uri = currentDirectory + currentChoice;
                StreamSocket socket = new StreamSocket();
                HostName h = new HostName("192.168.42.1");
                HostName i = new HostName("localhost");
                EndpointPair ep = new EndpointPair(i,"",h,"6789");

                await socket.ConnectAsync(h, "6789");
               
                DataWriter writer = new DataWriter(socket.OutputStream);
                writer.WriteString("FILE FETCH " + currentDirectory + currentChoice + '\n');
                await writer.StoreAsync();
                writer.DetachStream();
                writer.Dispose();
                DataReader reader = new DataReader(socket.InputStream);
                reader.InputStreamOptions = InputStreamOptions.None;
                byte[] receivedData = new byte[fileSizes.ElementAt(currentIndex)];

                //you need to keep track of bytes!
                //1024 at a time
                //when less than 1024 remains, transfer only the difference
                //have a counter to keep track of each transaction
                int size = fileSizes.ElementAt(currentIndex);
                List<byte> bytes = new List<byte>();
                while(true)
                {
                    if (size == 0)
                    {
                        break;
                    }
                    else if (size >= 10485760)
                    {
                        var count = await reader.LoadAsync(10485760);
                        byte[] temp = new byte[count];
                        reader.ReadBytes(temp);
                        foreach (byte b in temp)
                        {
                            bytes.Add(b);
                        }
                        size -= 10485760;
                    }
                    else
                    {
                        var count = await reader.LoadAsync((uint)size);
                       
                        byte[] temp = new byte[count];
                        
                        reader.ReadBytes(temp);
                        foreach (byte b in temp)
                        {
                            bytes.Add(b);
                        }
                        break;
                    } 
                }
                
                System.GC.Collect();
                byte[] file = bytes.ToArray();
                using (IsolatedStorageFileStream FS = new IsolatedStorageFileStream(currentChoice, FileMode.Create, IsolatedStorageFile.GetUserStoreForApplication()))
                {
                    FS.Write(file, 0, file.Length);
                }
               
                StorageFolder local = Windows.Storage.ApplicationData.Current.LocalFolder;
                StorageFile bqfile = await local.GetFileAsync(currentChoice);
                reader.DetachBuffer();
                reader.DetachStream();
                reader.Dispose();             
                //socket.Dispose(); 
                //socket = null;
                await Windows.System.Launcher.LaunchFileAsync(bqfile);
            }
        }

     
        public async Task WriteDataToFileAsync(string fileName, byte[] input)
        {
            byte[] data = input;

            var folder = ApplicationData.Current.LocalFolder;
            var file = await folder.CreateFileAsync(fileName, CreationCollisionOption.ReplaceExisting);

            using (var s = await file.OpenStreamForWriteAsync())
            {
                await s.WriteAsync(data, 0, data.Length);
            }
        }

        private void Back_Click(object sender, RoutedEventArgs e)
        {
            if (currentDirectory.Equals("/media/CLOUDETTE/") == false)
            {
                fileList.Items.Clear();
                currentDirectory = currentDirectory.Substring(0, currentDirectory.Length - 1);
                int index = currentDirectory.LastIndexOf('/');
                currentDirectory = currentDirectory.Substring(0, index+1);
                String temp = currentDirectory.Substring(0, currentDirectory.Length-1);
                int index2 = temp.LastIndexOf('/');
                String folderName = temp.Substring(index2+1, (index-1)-(index2));
                title.Text = folderName.ToUpper();
                getFolder(currentDirectory);
            }
        }

        //event listener for highlighting selected item
        //event listener for double tapping the item.
        //if folder, 
   
        
    }
}