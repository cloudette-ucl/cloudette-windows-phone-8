﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Navigation;
using Microsoft.Phone.Controls;
using Microsoft.Phone.Shell;
using System.Threading.Tasks;

namespace CloudetteWP8
{
    public partial class OneDriveAuth : PhoneApplicationPage
    {
        private const string url = "https://login.live.com/oauth20_authorize.srf?client_id=0000000048110B0F&scope=wl.signin%20wl.offline_access&response_type=code&redirect_uri=https://login.live.com/oauth20_desktop.srf";

        public OneDriveAuth()
        {
            InitializeComponent();
            browser.Navigate(new Uri(url)); 
            startListening();
            //need to resize browser window.
        }

        public static String authCode = string.Empty;

        private async void startListening()
        {
            while (true)
            {
                await Task.Delay(5000);
                if (browser.Source.AbsoluteUri.Contains("code=") == true)
                {
                    authCode = browser.Source.AbsoluteUri;
                    break;
                }
            }
            filterOutAuthCode();
            obtainTokens();
     
        }

        String response = string.Empty;
        WebClient webClient;
        private void obtainTokens()
        {

            webClient = new WebClient();
            webClient.DownloadStringCompleted += new DownloadStringCompletedEventHandler(webClient_DownloadStringCompleted);
            webClient.DownloadProgressChanged += new DownloadProgressChangedEventHandler(webClient_DownloadProgressChanged);
            webClient.DownloadStringAsync(new Uri("https://login.live.com/oauth20_token.srf?client_id=0000000048110B0F&redirect_uri=https://login.live.com/oauth20_desktop.srf&client_secret=WDbbFoJu74P9VSj0XUMFGT7c9Rbd0lTe&code=" + authCode + "&grant_type=authorization_code"));  
        }

        void webClient_DownloadProgressChanged(object sender, DownloadProgressChangedEventArgs e)
        {

        }

        void webClient_DownloadStringCompleted(object sender, DownloadStringCompletedEventArgs e)
        {
            if (e.Cancelled == true)
            {
                return;
            }
            
            if (e.Error != null)
            {
                return;
            }
            response = e.Result;
            
            int index = response.IndexOf("access_token");
            response = response.Substring(index);
            debug.Text = response;
            sendTokenToCloudette();
        }

        private void filterOutAuthCode()
        {
            int index1 = authCode.IndexOf("code=");
            int index2 = authCode.Length - 1;
            index1 += 5;
            authCode = authCode.Substring(index1, index2 - index1);
            
        }
   
        private async void sendTokenToCloudette()
        {
            TCPClient tcp = new TCPClient();
            tcp.sendSkyDrive("192.168.42.1", 6789, response.ToString());
            await Task.Delay(4000);
            NavigationService.Navigate(new Uri("/FileList.xaml", UriKind.RelativeOrAbsolute));
        }


    }
}